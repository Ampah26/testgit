from rest_framework import serializers
from base.models import Item,Variation,ItemVariation,OrderItem,Order


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = [
            'id',
            'title',
            'price',
            'discount_price',
            'slug',
            'status',
            'category',
            'labels',
            'description',
            'image',
        ]