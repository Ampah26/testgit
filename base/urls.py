from django.urls import path,include
from . import views

urlpatterns = [
    # path('', views.IndexView.as_view(), name='index'),
    path('', views.index, name='index'),

    path('checkout/', views.CheckoutView.as_view(), name='checkout'),
    path('payment/<payment_option>/', views.PaymentView.as_view(), name='payment'),
    path('confirmation/', views.confirmation, name='confirmation'),
    path('paysuccess/', views.paysuccess, name='paysuccess'),
    path('payfail/', views.payfail, name='payfail'),
    path('about/', views.about, name='about'),
    
    # path('confirmation/', views.ConfirmationView.as_view(), name='confirmation'),
    path('remove_from_cart/<slug>/', views.remove_from_cart, name='remove_from_cart'),
    path('add_to_cart/<slug>/', views.add_to_cart, name='add_to_cart'),
    path('contact/', views.contact, name='contact'),
    path('detail/<slug>/', views.DetailView.as_view(), name='detail'),
    path('cart/', views.Cart.as_view(), name='cart'),
    path('shop/', views.shop, name='shop'),
    path('hauts/', views.hauts, name='hauts'),
    path('robes/', views.robes, name='robes'),

    path('pantalon/', views.pantalon, name='pantalon'),
    path('jupes/', views.jupes, name='jupes'),

    path('update_profile/', views.update_profile, name='update_profile'),
    
 

]
