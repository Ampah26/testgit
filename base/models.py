from pickle import FALSE, TRUE
from django.db import models
from django.shortcuts import reverse
from django.contrib.auth.models import User
from django_countries.fields import CountryField
from django.db.models.signals import post_save
from django.dispatch import receiver

#une liste de liste
CATEGORY_CHOICES=(
    ('Rb', 'robe'),
    ('J', 'Jupe'),
    ('H', 'Haut'),
    ('C', 'Cargo'),
    ('Pt', 'Pantalon'),)

SIZE_CHOICES=(
    ('XS', 'extra petite'),
    ('S', 'petite'),
    ('M', 'moyenne'),
    ('L', 'large'),
    ('XL', 'extra large'),
    ('XL', 'très extra large'),
    )
LABEL_CHOICES=(
    ('NA', 'nouvel article'),
    ('SO', 'Vendu'),
)

PAYMENT_CHOICES = (
    ('FLOOZ', 'FLOOZ'),
    ('TMONEY', 'TMONEY'),
)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    numero = models.IntegerField(null=True, blank=False)
    birth_date = models.DateField(null=True, blank=True)

    def get_user_number(self):
        return self.numero

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class Item(models.Model):
    title = models.CharField(max_length=150)
    price = models.IntegerField()
    discount_price = models.IntegerField(blank=True, null=True)
    size=models.CharField(choices=SIZE_CHOICES, max_length=2,default="moyenne")
    slug = models.SlugField()
    category=models.CharField(choices=CATEGORY_CHOICES, max_length=2)
    labels=models.CharField(choices=LABEL_CHOICES, max_length=2)
    description=models.TextField(blank=True, null=True)
    image = models.ImageField(default='default.jpg',upload_to='static/images')
    image2 = models.ImageField(default='default.jpg',upload_to='static/images')
    image3 = models.ImageField(default='default.jpg',upload_to='static/images')


    def __str__(self):
        return self.title
    
    # def get_change_label(self):
    #     return self.quantity * self.item.price
         
    def get_add_to_cart_url(self):
        return reverse('add_to_cart', kwargs={'slug':self.slug})

    def get_remove_from_cart_url(self):
        return reverse('remove_from_cart', kwargs={'slug':self.slug})


class Variation(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)

    class Meta:
        unique_together = (
            ('item', 'name')
        )

    def __str__(self):
        return self.name

class ItemVariation(models.Model):
    variation = models.ForeignKey(Variation, on_delete=models.CASCADE)
    value = models.CharField(max_length=200)
    attachment = models.ImageField(upload_to="static/images", blank=True, null=True)

    class Meta:
        unique_together = (
            ('variation', 'value')
        )

    def __str__(self):
        return self.value

class OrderItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.quantity} of {self.item.title}"

    def get_total_item_price(self):
        return self.quantity * self.item.price
    


    def get_total_item_discount_price(self):
            return self.quantity * self.item.discount_price

    def get_amount_saved(self):
        return self.get_total_item_price() - self.get_final_price()


    def get_final_price(self):
        if self.item.discount_price:
            return self.get_total_item_discount_price()
        return self.get_total_item_price()

    
    
  
class Order(models.Model):
    user= models.ForeignKey(User, on_delete=models.CASCADE)
    items=models.ManyToManyField(OrderItem)
    address= models.ForeignKey("Address", on_delete=models.SET_NULL, blank=True, null=True)
    payment = models.ForeignKey("Payment", on_delete=models.SET_NULL, blank=True, null=True)
    ordered=models.BooleanField(default=False)
    start_date=models.DateTimeField(auto_now_add=True)
    ordered_date=models.DateTimeField()


    def __str__(self):
        return self.user.username

    def get_total(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_final_price()
        # if self.coupon:
        #     total -= self.coupon.amount
        return total
    

    def get_total_plus_shipping(self):
        return self.get_total() + 1000



class Address(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    street_address = models.CharField(max_length=200)
    apartment_address = models.CharField(max_length=200)
    country = CountryField(multiple=False, default="TG")
    zip = models.CharField(max_length=200)
    save_info = models.BooleanField(default=False)
    default = models.BooleanField(default=False)
    use_default = models.BooleanField(default=False)
    payment_option = models.CharField(choices=PAYMENT_CHOICES, max_length=20)

    class Meta:
        verbose_name_plural = 'Addresses'

    def __str__(self):
        return self.street_address + ',' + self.apartment_address

    def get_payment_option(self):
        return self.payment_option

    def get_street_address(self):
        return self.street_address

class Payment(models.Model):
    paymentId = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.CharField(max_length=100)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    
    
    

    def __str__(self):
        return str(self.paymentId)

    def get_id(user):
        return user.paymentId



class Contact(models.Model):
    name = models.CharField(max_length=150)
    numero = models.IntegerField()
    message=models.TextField(blank=True, null=True)


    def __str__(self):
        return self.name