
from django.shortcuts import render,get_object_or_404, redirect
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import ListView, DetailView, View
from .models import Item, OrderItem, Order,Address,Payment, Profile,Contact
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import AddressForm,UserForm,ProfileForm,PaymentForm,ContactForm

import json
from django.contrib.auth.models import User
from django.http import JsonResponse
import requests
from django.http import HttpResponse
from allauth.account.views import SignupView, LoginView
import random



def generate_id():
    """Generate a unique ID of at least 4 digits"""
    with open("used_ids.txt", "r") as f:
        used_ids = set(f.read().split("\n"))

    while True:
        # Generate a random integer between 1000 and 9999
        new_id = random.randint(1000, 9999)
        if new_id not in used_ids:
            break

    with open("used_ids.txt", "a") as f:
        f.write(str(new_id) + "\n")

    yield new_id
# class IndexView(ListView):
    
#     model = Item
#     item=model.objects.all()[:5]
#     template_name = 'base/index.html'

def index(request):
    # items=Item.objects.filter(category='H')[:5]
    # items=Item.objects.all()[:10]
    # items=Item.objects.random.choice(items)
    items=Item.objects.all()

    ma_liste=list()
    for i in range(20):
        randnum=random.choice(items)
        
        if randnum not in ma_liste:
            ma_liste.append(randnum)

    # items=Item.objects.all()
    context={
        'items':items,
        'ma_liste':ma_liste
        # 'randnum':randnum

    }
    return render(request, 'base/index.html',context)



def hauts(request):
    # items=Item.objects.filter(category='H')[:5]
    items=Item.objects.filter(category='H')
    ma_liste=list()
    for i in range(16):
        randnum=random.choice(items)
        
        if randnum not in ma_liste:
            ma_liste.append(randnum)
    # items=Item.objects.all()
    context={
        'items':items,
        'ma_liste':ma_liste
    }
    return render(request, 'base/hauts.html',context)


def pantalon(request):
    # items=Item.objects.filter(category='H')[:5]
    items=Item.objects.filter(category='Pt')
    ma_liste=list()
    for i in range(10):
        randnum=random.choice(items)
        
        if randnum not in ma_liste:
            ma_liste.append(randnum)
    # items=Item.objects.all()
    context={
        'items':items,
        'ma_liste':ma_liste
    }
    return render(request, 'base/pantalon.html',context)

def jupes(request):
    # items=Item.objects.filter(category='H')[:5]
    items=Item.objects.filter(category='J')
    ma_liste=list()
    for i in range(15):
        randnum=random.choice(items)
        
        if randnum not in ma_liste:
            ma_liste.append(randnum)
    # items=Item.objects.all()
    context={
        'items':items,
        'ma_liste':ma_liste
    }
    return render(request, 'base/jupes.html',context)


def robes(request):
    # items=Item.objects.filter(category='H')[:5]
    items=Item.objects.filter(category='Rb')
    ma_liste=list()
    for i in range(14):
        randnum=random.choice(items)
        
        if randnum not in ma_liste:
            ma_liste.append(randnum)
    # items=Item.objects.all()
    context={
        'items':items,
        'ma_liste':ma_liste
    }
    return render(request, 'base/robes.html',context)

# def about(request):
#     return render(request, 'base/about.html')
class DetailView(DetailView):
    
    model = Item
    template_name = 'base/detail.html'

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            numero = form.cleaned_data['numero']
            message = form.cleaned_data['message']
            contact = Contact(name=name, numero=numero, message=message)
            contact.save()
            return redirect('shop')
    else:
        form = ContactForm()
    return render(request, 'base/contact.html', {'form': form})


class CheckoutView(View):
    def get(self, *args, **kwargs):
        
        order = Order.objects.get(user=self.request.user, ordered=False)
        form = AddressForm()
        context = {
            'form': form,
            'order': order
        }
        return render(self.request, 'base/checkout.html', context)

    
    def post(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)
        form = AddressForm(self.request.POST or None)
        if form.is_valid():
            street_address = form.cleaned_data.get('street_address')
            apartment_address = form.cleaned_data.get('apartment_address')
            payment_option = form.cleaned_data.get('payment_option')
            

            address = Address(
                user=self.request.user,
                street_address=street_address,
                apartment_address=apartment_address,
                
            )

            address.save()
         
            order.address = address
            order.save()

            

            if payment_option == "FLOOZ":
                return redirect('payment', payment_option="FLOOZ")

            if payment_option == "TMONEY":
                return redirect('payment', payment_option="TMONEY")
            messages.info(self.request, "Invalid payment option")
            return redirect('checkout')
        else:
            print('form is invalid')
            return redirect('checkout')


def shop(request):

    items=Item.objects.all()
    # items=Item.objects.random.choice(items)

    ma_liste=list()
    for i in range(16):
        randnum=random.choice(items)
        
        if randnum not in ma_liste:
            ma_liste.append(randnum)

    # items=Item.objects.all()
    context={
        'items':items,
        'ma_liste':ma_liste
    }
    return render(request, 'base/shop.html',context)




@login_required
def add_to_cart(request, slug):
    item =get_object_or_404(Item, slug=slug)
    order_item, created= OrderItem.objects.get_or_create(item=item, user= request.user, ordered=False)
    order_qs=Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order=order_qs[0]
        if order.items.filter(item__slug=item.slug).exists():
            order_item.quantity=1
            # order_item.quantity +=1
            order_item.save()
            messages.success(request, f"{item.title}'s quantity was updated ")
            return redirect('cart')
        else:
            order.items.add(order_item)
            order.save()
            messages.success(request, f"{item.title} was added to your cart")
            return redirect('cart')
    else:
        ordered_date=timezone.now()
        order=Order.objects.create(user=request.user, ordered=False, ordered_date=ordered_date)
        order.items.add(order_item)
        order.save()
        messages.success(request, f"{item.title} was added to your cart")
        return redirect('cart')



@login_required
def remove_from_cart(request, slug):
    item =get_object_or_404(Item, slug=slug)
    order_item, created= OrderItem.objects.get_or_create(item=item, user= request.user, ordered=False)
    order_qs=Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order=order_qs[0]
        if order.items.filter(item__slug=item.slug).exists():
            order.items.remove(order_item)
            order.save()
            messages.success(request, f"{item.title} was removed from your cart ")
            return redirect('cart')
        else:
            messages.info(request, f"{item.title} was not in your your cart")
            return redirect('cart')
    else:
        messages.info(request, " you do  not have an active order")
        return redirect('cart')


class Cart(View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'order': order
            }
            return render(self.request, 'base/cart.html', context)
        except ObjectDoesNotExist:
             messages.success(self.request, "Votre panier est vide")
             return redirect('index')


def login(request):
    return render(request, 'account/login.html')

def already_logged_in(request):
    return render(request, 'account/snippets/already_logged_in.html')

def account_inactive(request):
    return render(request, 'account/account_inactive.html')

def email_confirm(request):
    return render(request, 'account/email_confirm.html')

def email(request):
    return render(request, 'account/email.html')

def logout(request):
    return render(request, 'account/logout.html')

def password_change(request):
    return render(request, 'account/password_change.html')

def password_reset_done(request):
    return render(request, 'account/password_reset_done.html')

def password_reset_from_key_done(request):
    return render(request, 'account/password_reset_from_key_done.html')

def password_reset_from_key(request):
    return render(request, 'account/password_reset_from_key.html')

def password_reset(request):
    return render(request, 'account/password_set.html')

def password_set(request):
    return render(request, 'account/password_set.html')

def signup_closed(request):
    return render(request, 'account/signup_closed.html')

def signup(request):
    return render(request, 'account/signup.html')

def verification_sent(request):
    return render(request, 'account/verification_sent.html')

def verified_email_required(request):
    return render(request, 'account/verified_email_required.html')

def payfail(request):
     return render(request, 'base/payfail.html')

def about(request):
     return render(request, 'base/about.html')
 
def confirmation(request):
    order = Order.objects.get(user=request.user, ordered=False)
    form = PaymentForm(request.POST or None)
    paymentId = Payment.objects.latest('paymentId')
    if form.is_valid():
            amount = form.cleaned_data.get('amount')
    # payment = Payment.objects.get()
    # payment_id=payment.get_id
    context = {
                 'order': order,
                 'paymentId':paymentId,
                #  'payment_id': payment_id
             }
    return render(request, 'base/confirmation.html',context)

def etat(request):
    with open('data.json') as mon_fichier:
        data=json.load(mon_fichier)
        print(data)
    return render(request, 'base/etatdupaiement.html')

    
def status(status):
    types={'0': 'Paiement réussi avec succès', '2' : 'En cours', '4': 'Expiré', '6': 'Annulé'}
    return types[str(status)]
    
def paysuccess(request):
    url ='https://paygateglobal.com/api/v2/status'
    
    order = Order.objects.get(user=request.user, ordered=False)
    # paymentsuccess = Paysuccess.objects.get(user=request.user)
    adresse = Address.objects.latest('street_address')
    amount = order.get_total_plus_shipping()
    identifier= Payment.objects.latest('paymentId')
    context = {
                'order': order,
             
            }
    params = dict(
          auth_token='593bd3ea-c9ae-4bc3-bb7b-d92cfaba604b',
          identifier= identifier,
        )
    res = requests.post(url,params=params)
    ct =res.json()
    st=status(ct['status'])
    if st=='Paiement réussi avec succès':
        print(st)
        payment = Payment(
                    user=request.user,
                    amount=amount,
                    address=adresse,

        )
        payment.save()
        order.ordered = True
        order.payment = payment
        order.save()
        for order_item in order.items.all():
            order_item.item.labels="SO"
            order_item.item.save()    
        return render(request,'base/paysuccess.html',context)
    #     return render(request,'base/paysuccess.html')
    elif st=='En cours':
         print(st)
         messages.info(request, " veuillez valider sur votre téléphone portable")
         return redirect('cart')
    elif st=='Expiré':
         print(st)
         messages.info(request, " Etat de votre paiement: Expiré; veuillez recommencer")
         return redirect('cart')
    elif st=='Annulé':
         print(st)
         messages.info(request, "Vous avez annulé votre commande")
         return redirect('cart')
    else:
        return redirect('payfail') 
        
    
     


class PaymentView(View):
    # auth_token= 593bd3ea-c9ae-4bc3-bb7b-d92cfaba604b
    def get(self, *args, **kwargs):
#         # try:
        order = Order.objects.get(user=self.request.user, ordered=False)
        form = PaymentForm()
        context = {
                'order': order,
                'form': form
             }
        
        return render(self.request, 'base/payment.html', context)
    
    def post(self, *args, **kwargs):
        
        order = Order.objects.get(user=self.request.user, ordered=False)
        adresse = Address.objects.latest('street_address')
        amount = order.get_total_plus_shipping()
        
        

        payment = Payment(
                    user=self.request.user,
                    amount=amount,
                    address=adresse,

                )
        payment.save()
        
        payment.paymentId = next(generate_id())
        payment.save()
        
        return redirect('confirmation')



@login_required
def update_profile(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, ('Your profile was successfully updated!'))
            return redirect('checkout')
        else:
            messages.error(request, ('Please correct the error below.'))
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'base/profile.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })






class MySignupView(SignupView):
    template_name = 'my_signup.html'


class MyLoginView(LoginView):
    template_name = 'my_login.html'