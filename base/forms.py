from django import forms
from django_countries.widgets import CountrySelectWidget
from django_countries.fields import CountryField
from .models import Profile,Contact
from django.contrib.auth.models import User

PAYMENT_CHOICES = (
    ('FLOOZ', 'FLOOZ'),
    ('TMONEY', 'TMONEY'),
)



class AddressForm(forms.Form):
    street_address = forms.CharField()
    apartment_address = forms.CharField()
    # country = CountryField(blank_label="Select country").formfield(widget=CountrySelectWidget(attrs={
    #     "class": "custom-select"
    # }))
    # zip = forms.CharField(required=False)
    # save_info = forms.BooleanField(required=False)
    # use_default = forms.BooleanField(required=False)
    payment_option = forms.ChoiceField(
        widget=forms.RadioSelect(), choices=PAYMENT_CHOICES)

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'email')

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('numero', 'birth_date')

class PaymentForm(forms.Form):
    numero= forms.CharField()
    amount = forms.CharField()
    
class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'numero', 'message']