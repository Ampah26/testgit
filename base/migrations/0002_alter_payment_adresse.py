# Generated by Django 4.1 on 2022-11-10 21:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='adresse',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='base.address'),
        ),
    ]
